# CCP Docker Project

Docker repositories for ccp micro services, currently the project contains postgres, dynamodb and elasticmq services, more services will be added in the future as needed.

> ## Getting Started

> Follow the steps below to start all the ccp docker containers in your local box

> 1. check out the docker project from bitbucket
> 1. `cd images`
> 1. enter command `docker-compose build` (if this is your first time running docker images, it will take couple of minutes to build the images)
> 1. once the images are successfully built, enter command `docker-compose up` to start the docker containers
> 1. once the containers are up and running, you can test their connectivites by `cd docker-test`, and run `mvn test`

> ## Prerequisites

> docker: you can find docker installation instruction on docker website

> ## Project Structure

> * there are two directories under the project
> * docker-test contains java tests which check the docker containers' connectivites
> * images contains Dockerfile and other configuration files for all docker images

> docker project includes three ccp modules, demo modules are under demo directory

> * images
>     * ccp-dynamodb
>     * ccp-elasticmq
>     * ccp-postgres
>     * docker-compose.yml: a compose file that includes ccp-dynamodb, ccp-elasticmq and ccp-postgres
>     * demo
>         * result-app
>         * voting-app
>         * worker-app
>         * docker-compose.yml: a compose file which demonstrates how containers can talk to each other via network
> * docker-test
>     * ccp-dynamodb
>     * ccp-elasticmq
>     * ccp-postgres

> note: sample projects are modified based on github project https://github.com/DaoCloud/example-voting-app

> ## How to Access Each Docker Service

> * sample web app http://localhost:5000 in the browser
> * postgres http://localhost:9432
> * elasticmq http://localhost:9324
> * dynamodb  http://localhost:12500 ( you can manipulate dynamodb via http://localhost:12500/shell/ in the browser)


> ## Manage Docker Images Using AWS Registry Container

> Instead of building docker images from your Docker file, developers can pull the latest images from AWS Registry Container

> > ### Prerequisites

> > 1. AWS dev account (Moose)
> > 1. AWS AMI credential is setup, the credential file is stored at /home/{username}/.aws/credential
> > 1. AWS Container Registry pull/push permission (Moose)
> > 1. docker and awscli installed in your dev environment


> > ### How to publish/pull docker images

> > 1. If you have multiple AWS profile, switch to correct profile using `export AWS_DEFAULT_PROFILE={aws_profile_name}`
> > 1. Retrieve the docker login command that you can use to authenticate your Docker client to your registry `aws ecr get-login`
> > 1. Run the docker login command that was returned in the previous step
> > 1. to push the docker image, run `docker push {aws_id}.dkr.ecr.us-west-2.amazonaws.com/{docker_image_name}:latest`
> > 1. to pull the docker image, run `docker pull {aws_id}.dkr.ecr.us-west-2.amazonaws.com/{docker_image_name}:latest`


> ## How do I Work With Docker Container

> Using ccp-postgres as example to demonstrate how to log onto docker container

> ccp-postgres extends public postgres docker project

> you can find customized Dockerfile and init.sql files under docker/ccp-postgres/

> * To build with the DockerFile, under the ccp-postgres directory
> > `docker build -t ccp-postgres .`
> * To remove the docker image - please make sure the image is not running
> > `docker rmi ccp-postgres`
> * To run the docker image
> > `docker run --rm -P -d -p 5432:5432 --name ccp-pg-container ccp-postgres`
> * to access postgres docker container (postgres needs to be installed in local env)
> > `psql -h localhost -p 5432 -U ccp`
> * to logon postgres docker container
> > `docker exec -it ccp-pg-container bash`
> * to query postgres database on docker container
> > `psql <database> <username>`

package ccp;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

import java.sql.Connection;
import java.sql.DriverManager;

import org.junit.Test;

public class postgres {

    @Test
    public void connect() throws Exception {
        Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/ccp_test", "ccp", "password");
        assertThat(connection, is(notNullValue()));
    }
}

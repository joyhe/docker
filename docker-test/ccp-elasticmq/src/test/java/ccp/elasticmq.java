package ccp;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.ListQueuesResult;

public class elasticmq {

    protected AmazonSQS createSQSClient() {
        AmazonSQSClient sqsClient = new AmazonSQSClient(new BasicAWSCredentials("x", "x"));
        sqsClient.setEndpoint("http://localhost:9324");
        return sqsClient;
    }

    @Test
    public void listQueues() {
        AmazonSQS sqsClient = createSQSClient();

        ListQueuesResult listQueuesResult = sqsClient.listQueues();
        List<String> queueUrls = listQueuesResult.getQueueUrls();
        Assert.assertEquals(3, queueUrls.size());
    }
}
